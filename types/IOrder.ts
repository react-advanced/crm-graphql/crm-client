import { IClient } from "./IClients";
import { IProduct } from "./IProduct";

export enum OrderStatus {
	PENDING = "PENDING",
	COMPLETED = "COMPLETED",
	CANCELED = "CANCELED",
}

export interface IProductQuantity {
	product: IProduct;
	quantity: number;
}

export interface IProductOrder {
	id: string;
	quantity: number;
}

export interface IOrder {
	id: string;
	productOrders: IProductOrder[];
	total: number;
	client: IClient;
	user: string;
	status: OrderStatus;
	createAt: string;
}

export interface IProductOrderInput {
	id: string;
	quantity: number;
}

export interface IOrderInput {
	productOrders: IProductOrderInput[];
	total: number;
	client: string;
	status: OrderStatus;
}

export enum UserRole {
	ADMIN = "ADMIN",
	USER = "USER",
}

export enum UserStatus {
	ACTIVE = "ACTIVE",
	INACTIVE = "INACTIVE",
}

export interface IUser {
	id?: string;
	firstname: string;
	lastname: string;
	email: string;
	role: UserRole;
	status: UserStatus;
	createAt?: string;
}

export interface IClient {
	id?: string;
	firstname: string;
	lastname: string;
	email: string;
	phone?: string;
	company: string;
	createAt?: string;
	seller?: string;
}

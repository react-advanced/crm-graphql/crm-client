export interface IAuth {
	accessToken: string;
	createAt: string;
	expiresAt: string;
}

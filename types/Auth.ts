export interface AuthResult {
	isAuthenticated: boolean;
	error: string;
	loading: boolean;
	token: string;

	login(input: LoginInput): Promise<void>;
	signup(input: SignUpInput): Promise<void>;
	logout(): void;
}

export interface LoginInput {
	email: string;
	password: string;
}

export interface SignUpInput {
	firstname: string;
	lastname: string;
	email: string;
	password: string;
}

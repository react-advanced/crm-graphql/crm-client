export interface IClientInput {
	firstname: string;
	lastname: string;
	email: string;
	phone?: string;
	company: string;
}

export interface FormInputProps {
	id: string;
	title: string;
	type: string;
	register: any;
	error?: string;
	autoComplete?: string;
	defaultValue?: any;
	step?: string
}

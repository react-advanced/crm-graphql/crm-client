import { useRouter } from "next/router";
import React from "react";
import Layout from "../components/layout/Layout";
import UsersTable from "../components/user/users-table";
import useDeleteUserAlert from "../hooks/alerts/useDeleteUserAlert";
import useDeleteUser from "../hooks/user/useDeleteUser";
import { userGetUsers } from "../hooks/user/useGetUsers";
import { IUser } from "../types/IUser";

const users = () => {
	const router = useRouter();
	const data = userGetUsers();
	const { deleteUser } = useDeleteUser();
	const { deleteUserAlert } = useDeleteUserAlert({ onConfirm: deleteUser });
	const onUpdateUser = (user: IUser) => router.push({ pathname: "/update-user/[id]", query: { id: user?.id } });
	const onDeleteUser = (user: IUser) => deleteUserAlert(user);

	return (
		<div>
			<Layout>
				<p className="text-2xl text-gray-800 font-light">Users</p>
				<UsersTable {...data} onUpdateUser={onUpdateUser} onDeleteUser={onDeleteUser} />
			</Layout>
		</div>
	);
};

export default users;

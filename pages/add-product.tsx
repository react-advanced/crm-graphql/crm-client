import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import Layout from "../components/layout/Layout";
import ProductForm from "../components/product/ProductForm";
import { useSaveProduct } from "../hooks/product/useSaveProduct";
import { IProduct } from "../types/IProduct";

const AddProduct = () => {
	const router = useRouter();
	const { saveProduct } = useSaveProduct();
	const [error, setError] = useState(null);

	const onSubmit = async (product: IProduct) => {
		const result = await saveProduct(product);
		if (result.error) setError(result.error);
		if (result.product) router.push("/products");
	};

	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Add Product</p>
			<Link href="/products">
				<a className="inline-block bg-blue-800 text-white rounded text-sm py-2 px-5 mt-3 hover:bg-gray-800 font-bold">Go Back</a>
			</Link>
			<ProductForm onSubmit={onSubmit} error={error} />
		</Layout>
	);
};

export default AddProduct;

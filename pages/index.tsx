import { useRouter } from "next/router";
import ClientsTable from "../components/client/clients-table";
import Layout from "../components/layout/Layout";
import LinkCircle from "../components/layout/LinkCircle";
import useDeleteClientAlert from "../hooks/alerts/useDeleteClientAlert";
import useDeleteClient from "../hooks/client/useDeleteClient";
import { useGetClients } from "../hooks/client/useGetClients";
import { IClient } from "../types/IClients";

const Index = () => {
	const router = useRouter();
	const data = useGetClients();
	const { deleteClient } = useDeleteClient();
	const { deleteClientAlert } = useDeleteClientAlert({ onConfirm: deleteClient });
	const handleUpdateClient = (client: IClient) => router.push({ pathname: "/update-client/[id]", query: { id: client.id } });

	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Clients</p>
			<ClientsTable {...data} onDeleteClient={deleteClientAlert} onUpdateClient={handleUpdateClient} />
			<div className="w-full flex justify-center py-5">
				<LinkCircle to="/add-client" />
			</div>
		</Layout>
	);
};

export default Index;

import { useRouter } from "next/router";
import React from "react";
import Layout from "../components/layout/Layout";
import LinkCircle from "../components/layout/LinkCircle";
import ProductsTable from "../components/product/products-table";
import useDeleteProductAlert from "../hooks/alerts/useDeleteProductAlert";
import useDeleteProduct from "../hooks/product/useDeleteProduct";
import { useGetProducts } from "../hooks/product/useGetProducts";
import { IProduct } from "../types/IProduct";

const Products = () => {
	const router = useRouter();
	const { products, isLoading, error } = useGetProducts();
	const { deleteProduct } = useDeleteProduct();
	const { deleteProductAlert } = useDeleteProductAlert({ onConfirm: deleteProduct });
	const onUpdateProduct = (product: IProduct) => router.push({ pathname: "/update-product/[id]", query: { id: product.id } });
	const onDeleteProduct = (product: IProduct) => deleteProductAlert(product);

	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Products</p>
			<ProductsTable products={products} isLoading={isLoading} error={error} onUpdateProduct={onUpdateProduct} onDeleteProduct={onDeleteProduct} />
			<div className="w-full flex justify-center py-5">
				<LinkCircle to="/add-product" />
			</div>
		</Layout>
	);
};

export default Products;

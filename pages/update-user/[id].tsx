import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import Layout from "../../components/layout/Layout";
import UserForm from "../../components/user/UserForm";
import { useGetUserById } from "../../hooks/user/useGetUserByID";
import { useUpdateUser } from "../../hooks/user/useUpdateUser";
import { IUser } from "../../types/IUser";

const UpdateUser = () => {
	const { query, push } = useRouter();
	const { updateUser } = useUpdateUser();
	const [submitError, setSubmitError] = useState(null);
	const { user, isLoading, error } = useGetUserById(query.id as string);

	const onSubmit = async (u: IUser) => {
		const { error: updateError } = await updateUser(query.id as string, u);
		if (updateError) {
			setSubmitError(updateError);
		} else {
			push("/users");
		}
	};

	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Update Client</p>
			<Link href="/users">
				<a className="inline-block bg-blue-800 text-white rounded text-sm py-2 px-5 mt-3 hover:bg-gray-800 font-bold">Go Back</a>
			</Link>
			{isLoading && <p>Loading...</p>}
			{error && <p>{error}</p>}
			{user && <UserForm onSubmit={onSubmit} error={submitError} initialValues={user} />}
		</Layout>
	);
};

export default UpdateUser;

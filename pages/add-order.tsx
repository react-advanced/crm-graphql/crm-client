import Link from "next/link";
import React from "react";
import Layout from "../components/layout/Layout";
import OrderForm from "../components/order/order-form";

const AddOrder = () => {
	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Add Order</p>
			<Link href="/orders">
				<a className="inline-block bg-blue-800 text-white rounded text-sm py-2 px-5 mt-3 hover:bg-gray-800 font-bold">Go Back</a>
			</Link>
			<OrderForm />
		</Layout>
	);
};

export default AddOrder;

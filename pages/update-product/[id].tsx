import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import Layout from "../../components/layout/Layout";
import ProductForm from "../../components/product/ProductForm";
import { useGetProduct } from "../../hooks/product/useGetProduct";
import { useUpdateProduct } from "../../hooks/product/useUpdateProduct";
import { IProduct } from "../../types/IProduct";

const UpdateProduct = () => {
	const { query, push } = useRouter();
	const { product, loading, error } = useGetProduct(query.id as string);
	const { updateProduct } = useUpdateProduct();
	const [submitError, setSubmitError] = useState(null);

	const onSubmit = async (p: IProduct) => {
		const { error: updateError } = await updateProduct(query.id as string, p);
		if (updateError) {
			setSubmitError(updateError);
		} else {
			push("/products");
		}
	};

	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Update Client</p>
			<Link href="/products">
				<a className="inline-block bg-blue-800 text-white rounded text-sm py-2 px-5 mt-3 hover:bg-gray-800 font-bold">Go Back</a>
			</Link>
			{loading && <p>Loading...</p>}
			{error && <p>{error}</p>}
			{product && <ProductForm initialValues={product} onSubmit={onSubmit} error={submitError} />}
		</Layout>
	);
};

export default UpdateProduct;

import React from "react";
import Layout from "../components/layout/Layout";
import LinkCircle from "../components/layout/LinkCircle";
import OrdersTable from "../components/order/orders-table";
import { useGetOrders } from "../hooks/orders/useGetOrders";

const Orders = () => {
	const result = useGetOrders();

	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Orders</p>
			<OrdersTable {...result} />
			<div className="w-full flex justify-center py-5">
				<LinkCircle to="/add-order" />
			</div>
		</Layout>
	);
};

export default Orders;

import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import ClientForm from "../../components/client/ClientForm";
import Layout from "../../components/layout/Layout";
import { useGetClient } from "../../hooks/client/useGetClient";
import { useUpdateClient } from "../../hooks/client/useUpdateClient";
import { IClient } from "../../types/IClients";

const EditClient = () => {
	const { query, push } = useRouter();
	const { client, loading, error } = useGetClient(query.id as string);
	const { updateClient } = useUpdateClient();
	const [submitError, setSubmitError] = useState(null);

	const onSubmit = async (c: IClient) => {
		const { error: updateError } = await updateClient(query.id as string, c);
		if (updateError) {
			setSubmitError(updateError);
		} else {
			push("/");
		}
	};

	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Update Client</p>
			<Link href="/">
				<a className="inline-block bg-blue-800 text-white rounded text-sm py-2 px-5 mt-3 hover:bg-gray-800 font-bold">Go Back</a>
			</Link>
			{loading && <p>Loading...</p>}
			{error && <p>{error}</p>}
			{client && <ClientForm initialValues={client} onSubmit={onSubmit} error={submitError} />}
		</Layout>
	);
};

export default EditClient;

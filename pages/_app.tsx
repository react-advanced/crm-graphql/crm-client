import ApolloCustomProvider from "../components/ApolloCustomProvider";
import { AuthenticationProvider } from "../components/auth/Authentication";
import ProtectRoute from "../components/auth/ProtectRoute";
import OrderProvider from "../components/order/order-provider";
import "../styles/globals.css";

const MyApp = ({ Component, pageProps }) => {
	return (
		<ApolloCustomProvider>
			<AuthenticationProvider>
				<ProtectRoute>
					<OrderProvider>
						<Component {...pageProps} />
					</OrderProvider>
				</ProtectRoute>
			</AuthenticationProvider>
		</ApolloCustomProvider>
	);
};

export default MyApp;

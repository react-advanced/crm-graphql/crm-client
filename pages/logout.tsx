import React, { useEffect } from "react";
import { useAuth } from "../components/auth/Authentication";
import LoadingScreen from "../components/layout/LoadingScreen";

const Logout = () => {
	const { logout } = useAuth();

	useEffect(() => logout(), []);

	return <LoadingScreen />;
};

export default Logout;

import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import ClientForm from "../components/client/ClientForm";
import Layout from "../components/layout/Layout";
import { useSaveClient } from "../hooks/client/useSaveClient";

const AddClient = () => {
	const router = useRouter();
	const { saveClient } = useSaveClient();
	const [error, setError] = useState(null);

	const onSubmit = async (data) => {
		const result = await saveClient(data);
		if (result.error) setError(result.error);
		if (result.client) router.push("/");
	};

	return (
		<Layout>
			<p className="text-2xl text-gray-800 font-light">Add Client</p>
			<Link href="/">
				<a className="inline-block bg-blue-800 text-white rounded text-sm py-2 px-5 mt-3 hover:bg-gray-800 font-bold">Go Back</a>
			</Link>
			<ClientForm onSubmit={onSubmit} error={error} />
		</Layout>
	);
};

export default AddClient;

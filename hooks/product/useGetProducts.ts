import { useQuery } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_PRODUCTS } from "../../graphql/products";
import { IProduct } from "../../types/IProduct";
import { mapProduct } from "../../utils/productUtils";

export interface GetProductsResult {
	products?: IProduct[];
	isLoading: boolean;
	error?: string;
}

export const useGetProducts = (): GetProductsResult => {
	const { token } = useAuth();
	const { data, loading, error } = useQuery(GET_PRODUCTS, {
		context: {
			headers: {
				Authorization: token,
			},
		},
	});

	return {
		products: data?.getProducts && data.getProducts.map((p) => mapProduct(p)),
		isLoading: loading,
		error: error?.message,
	};
};

import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_PRODUCTS, SAVE_PRODUCT } from "../../graphql/products";
import { IProduct } from "../../types/IProduct";

export interface SaveProductResult {
	product?: IProduct;
	error?: string;
}

export interface UserProductHookResult {
	saveProduct(product: IProduct): Promise<SaveProductResult>;
}

export const useSaveProduct = (): UserProductHookResult => {
	const { token } = useAuth();
	const [saveProduct] = useMutation(SAVE_PRODUCT, {
		context: {
			headers: {
				Authorization: token,
			},
		},
		update: (caches, { data: { saveProduct: product } }) => {
			const { getProducts } = caches.readQuery({ query: GET_PRODUCTS });

			caches.writeQuery({
				query: GET_PRODUCTS,
				data: { getProducts: [...getProducts, product] },
			});
		},
	});

	return {
		saveProduct: async (product) => {
			try {
				const { data } = await saveProduct({
					variables: {
						input: {
							name: product.name,
							stock: product.stock,
							price: product.price,
						},
					},
				});
				return { product: data?.saveProduct };
			} catch (e) {
				console.log(e);
				return { error: e.message };
			}
		},
	};
};

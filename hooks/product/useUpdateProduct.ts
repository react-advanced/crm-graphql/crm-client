import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_PRODUCTS, UPDATE_PRODUCT } from "../../graphql/products";
import { IProduct } from "../../types/IProduct";

export interface UpdateProductResult {
	product?: IProduct;
	error?: string;
}

export interface UpdateProductHookResult {
	updateProduct(id: string, product: IProduct): Promise<UpdateProductResult>;
}

export const useUpdateProduct = (): UpdateProductHookResult => {
	const { token } = useAuth();
	const [updateProduct] = useMutation(UPDATE_PRODUCT, {
		context: {
			headers: {
				Authorization: token,
			},
		},
		update: (caches, { data: { updateProduct: product } }) => {
			const { getProducts } = caches.readQuery({ query: GET_PRODUCTS });

			const update = getProducts.filter((p) => p.id !== product.id);
			update.push(product);

			caches.writeQuery({
				query: GET_PRODUCTS,
				data: { getProducts: update },
			});
		},
	});

	return {
		updateProduct: async (id, product) => {
			try {
				const { data } = await updateProduct({
					variables: {
						id,
						input: {
							name: product.name,
							stock: product.stock,
							price: product.price,
						},
					},
				});
				return { product: data?.product };
			} catch (e) {
				console.log(e);
				return { error: e.message };
			}
		},
	};
};

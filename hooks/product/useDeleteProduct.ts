import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { DELETE_PRODUCT, GET_PRODUCTS } from "../../graphql/products";
import { IProduct } from "../../types/IProduct";

export interface DeleteProductHookResult {
	deleteProduct(product: IProduct): Promise<void>;
}

const useDeleteProduct = () => {
	const { token } = useAuth();
	const [deleteProduct] = useMutation(DELETE_PRODUCT, {
		context: {
			headers: {
				Authorization: token,
			},
		},
		update: (caches, { data: { deleteProduct: product } }) => {
			const { getProducts } = caches.readQuery({ query: GET_PRODUCTS });

			caches.writeQuery({
				query: GET_PRODUCTS,
				data: { getProducts: getProducts.filter((p) => p.id !== product.id) },
			});
		},
	});

	return {
		deleteProduct: async (product: IProduct) => {
			await deleteProduct({ variables: { id: product.id } });
		},
	};
};

export default useDeleteProduct;

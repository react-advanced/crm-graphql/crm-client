import { useQuery } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_PRODUCT } from "../../graphql/products";
import { IProduct } from "../../types/IProduct";
import { mapProduct } from "../../utils/productUtils";

export interface GetProductResult {
	product?: IProduct;
	loading: boolean;
	error?: string;
}

export const useGetProduct = (id: string): GetProductResult => {
	const { token } = useAuth();

	const { data, loading, error } = useQuery(GET_PRODUCT, {
		variables: { id },
		context: {
			headers: {
				Authorization: token,
			},
		},
	});

	return {
		product: data?.getProduct && mapProduct(data.getProduct),
		loading,
		error: error?.message,
	};
};

import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_ORDERS_BY_USER, SAVE_ORDER } from "../../graphql/orders";
import { IOrder, IOrderInput } from "../../types/IOrder";

export interface SaveOrderResult {
	order?: IOrder;
	error?: string;
}

export interface SaverOrderHook {
	saveOrder(order: IOrderInput): Promise<SaveOrderResult>;
}

export const useSaveOrder = (): SaverOrderHook => {
	const { token } = useAuth();
	const [saveOrder] = useMutation(SAVE_ORDER, {
		context: {
			headers: {
				Authorization: token,
			},
		},
		update: (caches, { data }) => {
			const { getOrdersByUser } = caches.readQuery({ query: GET_ORDERS_BY_USER });
			caches.writeQuery({
				query: GET_ORDERS_BY_USER,
				data: { getOrdersByUser: [...getOrdersByUser, data.saveOrder] },
			});
		},
	});

	return {
		saveOrder: async (order: IOrder) => {
			try {
				const { data } = await saveOrder({ variables: { input: order } });
				return { order: data?.saveOrder };
			} catch (e) {
				return { error: e.message };
			}
		},
	};
};

import { useQuery } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_ORDERS_BY_USER } from "../../graphql/orders";
import { IOrder } from "../../types/IOrder";

export interface GetOrdersResult {
	orders?: IOrder[];
	isLoading: boolean;
	error?: string;
}

export const useGetOrders = (): GetOrdersResult => {
	const { token } = useAuth();
	const { data, loading, error } = useQuery(GET_ORDERS_BY_USER, {
		context: {
			headers: {
				Authorization: token,
			},
		},
	});

	return {
		orders: data?.getOrdersByUser,
		isLoading: loading,
		error: error?.message,
	};
};

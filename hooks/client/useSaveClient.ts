import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_CLIENTS, SAVE_CLIENT } from "../../graphql/clients";
import { IClientInput } from "../../types/IClientInput";
import { IClient } from "../../types/IClients";
import { mapClient } from "../../utils/clientUtils";

export interface SaveClientResult {
	client?: IClient;
	error?: string;
}

export interface SaveClientHook {
	saveClient(client: IClientInput): Promise<SaveClientResult>;
}

export const useSaveClient = (): SaveClientHook => {
	const { token } = useAuth();
	const [saveClient] = useMutation(SAVE_CLIENT, {
		update: (caches, { data: { saveClient: client } }) => {
			const { getClients } = caches.readQuery({ query: GET_CLIENTS });

			caches.writeQuery({
				query: GET_CLIENTS,
				data: { getClients: [...getClients, client] },
			});
		},
	});

	return {
		saveClient: async (client) => {
			try {
				const { data } = await saveClient({
					variables: { input: { ...client } },
					context: {
						headers: {
							Authorization: token,
						},
					},
				});

				return { client: data?.saveClient && mapClient(data.saveClient) };
			} catch (e) {
				console.log(e);
				return { error: e.message };
			}
		},
	};
};

import { useQuery } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_CLIENTS } from "../../graphql/clients";
import { IClient } from "../../types/IClients";
import { mapClient } from "../../utils/clientUtils";

export interface GetClientsResult {
	clients?: IClient[];
	isLoading: boolean;
	error?: string;
}

export const useGetClients = (): GetClientsResult => {
	const { token } = useAuth();

	const { data, loading, error } = useQuery(GET_CLIENTS, {
		context: {
			headers: {
				Authorization: token,
			},
		},
	});

	return {
		clients: data?.getClients && data.getClients.map((c) => mapClient(c)),
		isLoading: loading,
		error: error?.message,
	};
};

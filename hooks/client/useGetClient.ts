import { useQuery } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_CLIENT } from "../../graphql/clients";
import { IClient } from "../../types/IClients";
import { mapClient } from "../../utils/clientUtils";

export interface GetClientResult {
	client?: IClient;
	loading: boolean;
	error?: string;
}

export const useGetClient = (id: string): GetClientResult => {
	const { token } = useAuth();

	const { data, loading, error } = useQuery(GET_CLIENT, {
		variables: { id },
		context: {
			headers: {
				Authorization: token,
			},
		},
	});

	return {
		client: data?.getClient && mapClient(data.getClient),
		loading,
		error: error?.message,
	};
};

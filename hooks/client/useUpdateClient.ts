import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_CLIENTS, UPDATE_CLIENT } from "../../graphql/clients";
import { IClient } from "../../types/IClients";
import { mapClient } from "../../utils/clientUtils";

export interface UpdateClientResult {
	client?: IClient;
	error?: string;
}

export interface UpdateClientHook {
	updateClient(id: string, client: IClient): Promise<UpdateClientResult>;
}

export const useUpdateClient = (): UpdateClientHook => {
	const { token } = useAuth();
	const [updateClient] = useMutation(UPDATE_CLIENT, {
		context: {
			headers: {
				Authorization: token,
			},
		},
		update: (cache, { data }) => {
			const { getClients } = cache.readQuery({ query: GET_CLIENTS });

			const update = getClients.filter((c) => c.id !== data.updateClient.id);
			update.push(data.updateClient);

			cache.writeQuery({
				query: GET_CLIENTS,
				data: { getClients: update },
			});
		},
	});

	return {
		updateClient: async (id, client) => {
			try {
				const { data } = await updateClient({
					variables: {
						id,
						input: {
							firstname: client.firstname,
							lastname: client.lastname,
							email: client.email,
							phone: client.phone,
							company: client.company,
						},
					},
				});
				return { client: data?.updateClient && mapClient(data.updateClient) };
			} catch (e) {
				console.log(e);
				return { error: e.message };
			}
		},
	};
};

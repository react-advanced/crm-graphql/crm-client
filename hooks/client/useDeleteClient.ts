import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { DELETE_CLIENT, GET_CLIENTS } from "../../graphql/clients";
import { IClient } from "../../types/IClients";

export interface DeleteClientHookResult {
	deleteClient(client: IClient): Promise<void>;
}

const useDeleteClient = () => {
	const { token } = useAuth();

	const [deleteClient] = useMutation(DELETE_CLIENT, {
		context: {
			headers: {
				Authorization: token,
			},
		},
		update: (caches, { data: { deleteClient: client } }) => {
			const { getClients } = caches.readQuery({ query: GET_CLIENTS });

			caches.writeQuery({
				query: GET_CLIENTS,
				data: { getClients: getClients.filter((c) => c.id !== client.id) },
			});
		},
	});

	return {
		deleteClient: async (client: IClient) => {
			await deleteClient({ variables: { id: client.id } });
		},
	};
};

export default useDeleteClient;

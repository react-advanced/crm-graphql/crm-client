import Swal from "sweetalert2";
import { IUser } from "../../types/IUser";

export interface DeleteUserAlertProps {
	onConfirm(user: IUser): Promise<void>;
	onCancel?(): Promise<void>;
}

export interface DeleteUserAlertResult {
	deleteUserAlert(user: IUser): Promise<void>;
}

const useDeleteUserAlert = ({ onConfirm }: DeleteUserAlertProps): DeleteUserAlertResult => {
	const deleteUserAlert = async (user: IUser) => {
		const { isConfirmed } = await Swal.fire({
			title: "Alert!",
			html:
				`<p>You are about to delete <strong>${user.firstname} ${user.lastname}</strong>` +
				`, email <strong>${user.email}</strong>.</p>` +
				"<p>Are you sure you want to delete this user?</p>",
			icon: "warning",
			cancelButtonText: "Cancel",
			confirmButtonText: "Yes, delete",
			confirmButtonColor: "red",
			showCancelButton: true,
		});

		if (isConfirmed) {
			try {
				await onConfirm(user);
				Swal.fire("Deleted!", "User has been deleted.", "success");
			} catch (e) {
				Swal.fire("Error!", `<p>User could not be deleted.</p><p>${e.message}</p>`, "error");
			}
		}
	};

	return { deleteUserAlert };
};

export default useDeleteUserAlert;

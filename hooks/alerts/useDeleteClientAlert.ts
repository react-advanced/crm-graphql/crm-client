import Swal from "sweetalert2";
import { IClient } from "../../types/IClients";

export interface DeleteClientAlertProps {
	onConfirm(client: IClient): Promise<void>;
	onCancel?(): Promise<void>;
}

export interface DeleteClientAlertResult {
	deleteClientAlert(client: IClient): Promise<void>;
}

const useDeleteClientAlert = ({ onConfirm }: DeleteClientAlertProps): DeleteClientAlertResult => {
	const deleteClientAlert = async (client: IClient) => {
		const { isConfirmed } = await Swal.fire({
			title: "Alert!",
			html:
				`<p>You are about to delete <strong>${client.firstname} ${client.lastname}</strong>` +
				`, email <strong>${client.email}</strong>.</p>` +
				"<p>Are you sure you want to delete this client?</p>",
			icon: "warning",
			cancelButtonText: "Cancel",
			confirmButtonText: "Yes, delete",
			confirmButtonColor: "red",
			showCancelButton: true,
		});

		if (isConfirmed) {
			try {
				await onConfirm(client);
				Swal.fire("Deleted!", "Your client has been deleted.", "success");
			} catch (e) {
				Swal.fire("Error!", `<p>Your client could not be deleted.</p><p>${e.message}</p>`, "error");
			}
		}
	};

	return { deleteClientAlert };
};

export default useDeleteClientAlert;

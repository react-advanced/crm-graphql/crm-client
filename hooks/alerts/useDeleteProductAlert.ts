import Swal from "sweetalert2";
import { IProduct } from "../../types/IProduct";

export interface DeleteProductAlertProps {
	onConfirm(product: IProduct): Promise<void>;
	onCancel?(): Promise<void>;
}

export interface DeleteProductAlertResult {
	deleteProductAlert(product: IProduct): Promise<void>;
}

const useDeleteProductAlert = ({ onConfirm }: DeleteProductAlertProps): DeleteProductAlertResult => {
	const deleteProductAlert = async (product: IProduct) => {
		const { isConfirmed } = await Swal.fire({
			title: "Alert!",
			html: `<p>You are about to delete <strong>${product.name}</strong>.` + "<p>Are you sure you want to delete this product?</p>",
			icon: "warning",
			cancelButtonText: "Cancel",
			confirmButtonText: "Yes, delete",
			confirmButtonColor: "red",
			showCancelButton: true,
		});

		if (isConfirmed) {
			try {
				await onConfirm(product);
				Swal.fire("Deleted!", "The product has been deleted.", "success");
			} catch (e) {
				Swal.fire("Error!", `<p>The product could not be deleted.</p><p>${e.message}</p>`, "error");
			}
		}
	};

	return { deleteProductAlert };
};

export default useDeleteProductAlert;

import Swal from "sweetalert2";
import { IOrder } from "../../types/IOrder";

export interface SaveOrderAlertProps {
	onConfirm(order: IOrder): Promise<void>;
	onCancel?(): Promise<void>;
}

export interface SaveOrderAlertResult {
	saveOrderAlert(order: IOrder): Promise<void>;
}

const useSaveOrderAlert = ({ onConfirm }: SaveOrderAlertProps): SaveOrderAlertResult => {
	const saveOrderAlert = async (order: IOrder) => {
		const { isConfirmed } = await Swal.fire({
			title: "Alert!",
			html: "Sure to confirm the purchase order?",
			icon: "question",
			cancelButtonText: "Cancel",
			confirmButtonText: "Yes, confirm",
			confirmButtonColor: "green",
			showCancelButton: true,
		});

		if (isConfirmed) {
			try {
				await onConfirm(order);
				Swal.fire("Success!", "Purchase order has been generated successfully.", "success");
			} catch (e) {
				Swal.fire("Error!", `<p>The purchase order could not be generated.</p><p>${e.message}</p>`, "error");
			}
		}
	};

	return { saveOrderAlert };
};

export default useSaveOrderAlert;

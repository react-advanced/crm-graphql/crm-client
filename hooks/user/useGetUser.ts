import { useQuery } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_USER } from "../../graphql/users";
import { IUser } from "../../types/IUser";
import { mapUser } from "../../utils/userUtils";

export interface GetUserResult {
	user?: IUser;
	isLoading: boolean;
	error?: string;
}

export const useGetUser = (): GetUserResult => {
	const { token } = useAuth();
	const { data, loading, error } = useQuery(GET_USER, {
		context: {
			headers: {
				Authorization: token,
			},
		},
	});

	return {
		user: data?.getUser && mapUser(data.getUser),
		isLoading: loading,
		error: error?.message,
	};
};

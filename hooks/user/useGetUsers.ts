import { useQuery } from "@apollo/client";
import moment from "moment";
import { useAuth } from "../../components/auth/Authentication";
import { GET_USERS } from "../../graphql/users";
import { IUser } from "../../types/IUser";

export interface GetUsersResult {
	users?: IUser[];
	isLoading: boolean;
	error?: string;
}

const mapUsers = (users: IUser[]): IUser[] => users.map((u) => ({ ...u, createAt: moment(parseInt(u.createAt)).format("DD-MM-YYYY hh:mm:ss") }));

export const userGetUsers = (): GetUsersResult => {
	const { token } = useAuth();

	const { data, loading, error } = useQuery(GET_USERS, {
		context: {
			headers: {
				Authorization: token,
			},
		},
	});

	return {
		users: data?.getUsers && mapUsers(data?.getUsers),
		isLoading: loading,
		error: error?.message,
	};
};

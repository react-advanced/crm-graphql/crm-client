import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_USERS, UPDATE_USER } from "../../graphql/users";
import { IUser } from "../../types/IUser";
import { mapUser } from "../../utils/userUtils";

export interface UpdateUserResult {
	user?: IUser;
	error?: string;
}

export interface UpdateUserHook {
	updateUser(id: string, user: IUser): Promise<UpdateUserResult>;
}

export const useUpdateUser = (): UpdateUserHook => {
	const { token } = useAuth();
	const [updateUser] = useMutation(UPDATE_USER, {
		context: {
			headers: {
				Authorization: token,
			},
		},
		update: (cache, { data }) => {
			const { getUsers } = cache.readQuery({ query: GET_USERS });

			const update = getUsers.filter((u) => u.id !== data.updateUser.id);
			update.push(data.updateUser as IUser);

			cache.writeQuery({
				query: GET_USERS,
				data: { getUsers: update },
			});
		},
	});

	return {
		updateUser: async (id, user) => {
			try {
				const { data } = await updateUser({
					variables: {
						id,
						input: {
							firstname: user.firstname,
							lastname: user.lastname,
							email: user.email,
							role: user.role,
							status: user.status,
						},
					},
				});
				return { user: data?.updateUser && mapUser(data.updateUser) };
			} catch (e) {
				console.log(e);
				return { error: e.message };
			}
		},
	};
};

import { useMutation } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { DELETE_USER, GET_USERS } from "../../graphql/users";
import { IUser } from "../../types/IUser";

export interface DeleteUserHookResult {
	deleteUser(user: IUser): Promise<void>;
}

const useDeleteUser = () => {
	const { token } = useAuth();

	const [deleteUser] = useMutation(DELETE_USER, {
		context: {
			headers: {
				Authorization: token,
			},
		},
		update: (caches, { data: { deleteUser: user } }) => {
			const { getUsers } = caches.readQuery({ query: GET_USERS });

			caches.writeQuery({
				query: GET_USERS,
				data: { getUsers: getUsers.filter((u) => u.id !== user.id) },
			});
		},
	});

	return {
		deleteUser: async (user: IUser) => {
			await deleteUser({ variables: { id: user.id } });
		},
	};
};

export default useDeleteUser;

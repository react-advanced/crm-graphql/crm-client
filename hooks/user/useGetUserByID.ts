import { useQuery } from "@apollo/client";
import { useAuth } from "../../components/auth/Authentication";
import { GET_USER_BY_ID } from "../../graphql/users";
import { IUser } from "../../types/IUser";
import { mapUser } from "../../utils/userUtils";

export interface GetUserByIdResult {
	user?: IUser;
	isLoading: boolean;
	error?: string;
}

export const useGetUserById = (id: string): GetUserByIdResult => {
	const { token } = useAuth();
	const { data, loading, error } = useQuery(GET_USER_BY_ID, {
		variables: { id },
		context: {
			headers: {
				Authorization: token,
			},
		},
	});

	return {
		user: data?.getUserById && mapUser(data.getUserById),
		isLoading: loading,
		error: error?.message,
	};
};

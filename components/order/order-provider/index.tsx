import React, { useContext, useReducer } from "react";
import { IClient } from "../../../types/IClients";
import { OrderStatus } from "../../../types/IOrder";
import { IProduct } from "../../../types/IProduct";
import { ADD_PRODUCT, REMOVE_PRODUCT, RESET, SET_CLIENT, UPDATE_QUANTITY } from "./actions";
import OrderContext, { IOrderContext } from "./context";
import OrderReducer from "./reducer";

export const useOrder = () => useContext(OrderContext) as IOrderContext;

const initialState = {
	quantities: [],
	order: { client: "", productOrders: [], status: OrderStatus.PENDING, total: 0 },
};

const OrderProvider = ({ children }) => {
	const [state, dispatch] = useReducer(OrderReducer, initialState);

	const reset = () => dispatch({ type: RESET, payload: initialState });
	const setClient = (client: IClient) => dispatch({ type: SET_CLIENT, payload: client });
	const addProduct = (product: IProduct) => dispatch({ type: ADD_PRODUCT, payload: product });
	const removeProduct = (product: IProduct) => dispatch({ type: REMOVE_PRODUCT, payload: product });
	const updateQuantity = (product: IProduct, quantity: number) => dispatch({ type: UPDATE_QUANTITY, payload: { product, quantity } });

	return <OrderContext.Provider value={{ state, reset, setClient, addProduct, removeProduct, updateQuantity }}>{children}</OrderContext.Provider>;
};

export default OrderProvider;

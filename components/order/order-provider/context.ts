import { createContext } from "react";
import { IClient } from "../../../types/IClients";
import { IOrderInput, IProductQuantity } from "../../../types/IOrder";
import { IProduct } from "../../../types/IProduct";

export interface IOrderContextState {
	client?: IClient;
	quantities: IProductQuantity[];
	order: IOrderInput;
}

export interface IOrderContext {
	state: IOrderContextState;
	reset();
	setClient(client: IClient);
	addProduct(product: IProduct);
	removeProduct(product: IProduct);
	updateQuantity(product: IProduct, quantity: number);
}

const OrderContext = createContext({});

export default OrderContext;

import { IClient } from "../../../types/IClients";
import { IProductQuantity } from "../../../types/IOrder";
import { IProduct } from "../../../types/IProduct";
import { ADD_PRODUCT, REMOVE_PRODUCT, RESET, SET_CLIENT, UPDATE_QUANTITY } from "./actions";
import { IOrderContextState } from "./context";

const calculateTotal = (quantities: IProductQuantity[]) => quantities.reduce((prev, current) => prev + current.product.price * current.quantity, 0);

const setClient = (state: IOrderContextState, client: IClient) => ({
	...state,
	client,
	order: { ...state.order, client: client.id },
});

const addProduct = (state: IOrderContextState, product: IProduct) => {
	const productOrder = { id: product.id, quantity: 1 };
	const order = { ...state.order, productOrders: [...state.order.productOrders, productOrder] };
	const quantities = [...state.quantities, { product: product, quantity: 1 }];
	order.total = calculateTotal(quantities);
	return {
		...state,
		quantities,
		order,
	};
};

const removeProduct = (state: IOrderContextState, product: IProduct) => {
	const productOrders = state.order.productOrders.filter((p) => p.id != product.id);
	const order = { ...state.order, productOrders };
	const quantities = state.quantities.filter((p) => p.product.id != product.id);
	order.total = calculateTotal(quantities);
	return {
		...state,
		quantities,
		order,
	};
};

const updateQuantity = (state: IOrderContextState, product: IProduct, quantity: number) => {
	if (Math.abs(quantity) > product.stock) return state;

	const productOrders = state.order.productOrders.filter((p) => p.id != product.id);
	productOrders.push({ id: product.id, quantity });
	const order = { ...state.order, productOrders };
	const quantities = state.quantities.filter((q) => q.product.id != product.id);
	quantities.push({ product, quantity });
	order.total = calculateTotal(quantities);

	return {
		...state,
		quantities,
		order,
	};
};

const OrderReducer = (state: IOrderContextState, { type, payload }) => {
	switch (type) {
		case SET_CLIENT:
			return setClient(state, payload);
		case ADD_PRODUCT:
			return addProduct(state, payload);
		case REMOVE_PRODUCT:
			return removeProduct(state, payload);
		case UPDATE_QUANTITY:
			return updateQuantity(state, payload.product, payload.quantity);
		case RESET:
			return { ...payload };
		default:
			console.error("INVALID ACTION TYPE: ", type);
			return state;
	}
};

export default OrderReducer;

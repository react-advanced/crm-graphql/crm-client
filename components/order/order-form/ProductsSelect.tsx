import React from "react";
import Select, { ActionMeta, ValueType } from "react-select";
import { IProduct } from "../../../types/IProduct";
import { useOrder } from "../order-provider";

export interface ClientSelectProps {
	products: IProduct[];
}

const ProductsSelect = ({ products }: ClientSelectProps) => {
	const {
		addProduct,
		removeProduct,
		state: { quantities },
	} = useOrder();

	const onChange = (value: ValueType<IProduct>, action: ActionMeta<IProduct>) => {
		switch (action.action) {
			case "select-option":
				addProduct(action.option);
				break;
			case "remove-value":
				removeProduct(action.removedValue);
				break;
			case "clear":
				products.forEach(removeProduct);
		}
	};

	return (
		<div className="w-full my-3">
			<label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="products">
				Products
			</label>
			<Select
				isMulti
				options={products.filter((p) => p.stock > 0)}
				closeMenuOnSelect={false}
				placeholder="Select client..."
				getOptionValue={(product) => product.id}
				getOptionLabel={(product) => `${product.name} - Stock: ${product.stock}`}
				defaultValue={quantities.map((q) => q.product)}
				onChange={onChange}
			/>
		</div>
	);
};

export default ProductsSelect;

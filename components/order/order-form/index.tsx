import { useRouter } from "next/router";
import React, { useState } from "react";
import { useGetClients } from "../../../hooks/client/useGetClients";
import { useSaveOrder } from "../../../hooks/orders/useSaveOrder";
import { useGetProducts } from "../../../hooks/product/useGetProducts";
import { useOrder } from "../order-provider";
import ClientSelect from "./ClientSelect";
import ProductOrders from "./ProductOrders";
import ProductsSelect from "./ProductsSelect";
import Total from "./Total";

const OrderForm = () => {
	const router = useRouter();
	const { state, reset } = useOrder();
	const [error, setError] = useState(null);
	const { clients, isLoading: loadingClients, error: clientsError } = useGetClients();
	const { products, isLoading: loadingProducts, error: productsError } = useGetProducts();
	const { saveOrder } = useSaveOrder();
	const onSubmit = async (e) => {
		e.preventDefault();
		const result = await saveOrder(state.order);
		if (result.error) setError(result.error);
		if (result.order) {
			reset();
			router.push("/orders");
		}
	};

	const isValidForm = (): boolean => {
		return state.quantities.length > 0 && !!state.client;
	};

	if (loadingClients || loadingProducts) return <p>Loading...</p>;

	if (clientsError || productsError) {
		return (
			<>
				{clientsError && <p>{clientsError}</p>}
				{productsError && <p>{productsError}</p>}
			</>
		);
	}

	return (
		<div className="flex justify-center mt-5">
			<div className="w-full max-w-lg">
				<form className="bg-white shadow-md px-8 pt-6 pb-8 mb-4" onSubmit={onSubmit}>
					<ClientSelect clients={clients} />
					<ProductsSelect products={products} />
					<ProductOrders />
					<Total />

					{error && (
						<div className="text-left text-sm mt-5 text-red-600">
							<span>{error}</span>
						</div>
					)}

					<input
						id="submit"
						type="submit"
						autoComplete="on"
						disabled={!isValidForm()}
						className={`${
							!isValidForm() 
							? "cursor-not-allowed bg-green-300"
							: "cursor-pointer bg-green-500 hover:bg-green-400"
						} w-full text-white rounded uppercase font-medium mt-5 p-2`}
						value="Buy"
					/>
				</form>
			</div>
		</div>
	);
};

export default OrderForm;

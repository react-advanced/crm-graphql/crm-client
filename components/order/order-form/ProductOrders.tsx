import React from "react";
import { IProduct } from "../../../types/IProduct";
import { toCurrency } from "../../../utils/currencyUtils";
import { sortProductQuantities } from "../../../utils/productUtils";
import { useOrder } from "../order-provider";

interface ProductOrderProps {
	product: IProduct;
	quantity: number;
	updateQuantity(product: IProduct, quantity: number);
}

const ProductOrder = ({ product, quantity, updateQuantity }: ProductOrderProps) => (
	<div className="flex w-full my-3 py-2 px-3 border rounded border-gray-400">
		<div className="w-3/5 text-left">
			<h1 className="text-xl px-3 py-1">{product.name}</h1>
			<span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 m-1">
				Unit price {toCurrency(product.price)}
			</span>
			<span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 m-1">
				Sub total {toCurrency(product.price * quantity)}
			</span>
		</div>
		<div className="w-2/5 flex flex-col justify-center items-center">
			<span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 m-1">Quantity: {quantity}</span>
			<div className="inline-block text-xs">
				<button
					type="button"
					onClick={() => updateQuantity(product, quantity - 1)}
					disabled={quantity <= 1}
					className={`${
						quantity <= 1 ? "bg-gray-300 hover:bg-gray-300 text-gray-700 cursor-not-allowed" : "bg-gray-300 hover:bg-gray-400 text-gray-800"
					}  font-bold py-1 px-3 rounded-l`}
				>
					-
				</button>
				<button
					type="button"
					onClick={() => updateQuantity(product, quantity + 1)}
					className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-3 rounded-r"
				>
					+
				</button>
			</div>
		</div>
	</div>
);

const ProductOrders = () => {
	const { state, updateQuantity } = useOrder();

	const productOrders = sortProductQuantities(state.quantities);

	return (
		<div className="w-full flex flex-col">
			{productOrders.map((q) => (
				<ProductOrder key={q.product.id} product={q.product} quantity={q.quantity} updateQuantity={updateQuantity} />
			))}
		</div>
	);
};

export default ProductOrders;

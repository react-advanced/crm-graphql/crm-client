import React from "react";
import { toCurrency } from "../../../utils/currencyUtils";
import { useOrder } from "../order-provider";

const Total = () => {
	const { state } = useOrder();
	const total = state.quantities.reduce((prev, current) => prev + current.product.price * current.quantity, 0);

	return (
		<div className="flex items-center mt-5 justify-between bg-white p-3 border border-gray-400 rounded">
			<h2 className="text-gray-800 text-lg">Total</h2>
			<p className="text-gray-800">{toCurrency(total)}</p>
		</div>
	);
};

export default Total;

import React from "react";
import Select, { ActionMeta, ValueType } from "react-select";
import { IClient } from "../../../types/IClients";
import { useOrder } from "../order-provider";

export interface ClientSelectProps {
	clients: IClient[];
}

const ClientSelect = ({ clients }: ClientSelectProps) => {
	const { setClient, state } = useOrder();

	const onChange = (value: ValueType<IClient>, action: ActionMeta<IClient>) => {
		if (action.action === "select-option") setClient(value as IClient);
	};

	return (
		<div className="w-full my-3">
			<label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="products">
				Client
			</label>
			<Select
				options={clients}
				onChange={onChange}
				placeholder="Select client..."
				defaultValue={state?.client}
				getOptionLabel={(client) => client.email}
				getOptionValue={(client) => client.id}
			/>
		</div>
	);
};

export default ClientSelect;

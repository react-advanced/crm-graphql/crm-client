import React from "react";
import { IOrder } from "../../../types/IOrder";
import { toCurrency } from "../../../utils/currencyUtils";
import { parseDate } from "../../../utils/dateUtils";

export interface OrderRowProps {
	order: IOrder;
}

const OrderRow = ({ order: { id, client, total, status, createAt } }: OrderRowProps) => {
	return (
		<tr>
			<td className="border px-4 py-2">{id}</td>
			<td className="border px-4 py-2">{`${client.firstname} ${client.lastname}`}</td>
			<td className="border px-4 py-2">{client.email}</td>
			<td className="border px-4 py-2">{toCurrency(total)}</td>
			<td className="border px-4 py-2">{status}</td>
			<td className="border px-4 py-2">{parseDate(createAt)}</td>
		</tr>
	);
};

export default OrderRow;

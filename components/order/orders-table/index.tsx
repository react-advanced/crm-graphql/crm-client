import React from "react";
import { IOrder } from "../../../types/IOrder";
import OrderRow from "./OrderRow";

export interface OrderTableProps {
	orders?: IOrder[];
	isLoading: boolean;
	error?: string;
}

const OrdersTable = ({ orders, isLoading, error }: OrderTableProps) => {
	if (isLoading) return <p>Loading...</p>;

	if (error) return <p>Error: {error}</p>;

	return (
		<table className="table-auto shadow-md mt-10 w-full w-lg">
			<thead className="bg-gray-800">
				<tr className="text-white">
					<th className="w-1/7 py-2">Number</th>
					<th className="w-1/7 py-2">Client</th>
					<th className="w-1/7 py-2">Email</th>
					<th className="w-1/7 py-2">Price</th>
					<th className="w-1/7 py-2">Status</th>
					<th className="w-2/7 py-2">CreateAt</th>
				</tr>
			</thead>
			<tbody className="bg-white">
				{orders.map((order) => (
					<OrderRow key={order.id} order={order} />
				))}
			</tbody>
		</table>
	);
};

export default OrdersTable;

import React from "react";
import { IProduct } from "../../../types/IProduct";
import ProductRow from "./ProductRow";

export interface ProductsTableProps {
	products?: IProduct[];
	isLoading: boolean;
	error?: string;

	onDeleteProduct(product: IProduct);
	onUpdateProduct(product: IProduct);
}

const ProductsTable = ({ products, isLoading, error, onUpdateProduct, onDeleteProduct }: ProductsTableProps) => {
	if (isLoading) return <p>Loading...</p>;

	if (error) return <p>Error: {error}</p>;

	return (
		<table className="table-auto shadow-md mt-10 w-full w-lg">
			<thead className="bg-gray-800">
				<tr className="text-white">
					<th className="w-1/5 py-2">Name</th>
					<th className="w-1/5 py-2">Stock</th>
					<th className="w-1/5 py-2">Price</th>
					<th className="w-1/5 py-2">CreateAt</th>
					<th className="w-1/12 py-2">Update</th>
					<th className="w-1/12 py-2">Delete</th>
				</tr>
			</thead>
			<tbody className="bg-white">
				{products &&
					products.map((p) => <ProductRow key={p.id} product={p} onUpdateProduct={onUpdateProduct} onDeleteProduct={onDeleteProduct} />)}
			</tbody>
		</table>
	);
};

export default ProductsTable;

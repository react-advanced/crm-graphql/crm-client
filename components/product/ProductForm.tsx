import React from "react";
import { useForm } from "react-hook-form";
import { IProduct } from "../../types/IProduct";
import FormInput from "../layout/FormInput";

interface ProductFormInputs {
	name: string;
	stock: string;
	price: string;
}

export interface ProductFormProps {
	initialValues?: IProduct;
	error?: string;
	onSubmit(produc: IProduct): Promise<void>;
}

const ProductForm = ({ initialValues = { name: "", price: 0, stock: 0 }, error, onSubmit }: ProductFormProps) => {
	const { register, handleSubmit, errors } = useForm();
	const onSubmitForm = ({ name, stock, price }: ProductFormInputs) =>
		onSubmit({ ...initialValues, name, stock: parseInt(stock), price: parseFloat(price) });

	return (
		<div className="flex justify-center mt-5">
			<div className="w-full max-w-lg">
				<form className="bg-white shadow-md px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit(onSubmitForm)}>
					<FormInput
						id="name"
						type="text"
						title="Name"
						error={errors?.name?.message}
						defaultValue={initialValues.name}
						register={register({
							required: { value: true, message: "The first name is required" },
							min: { value: 6, message: "Must be at least 6 long" },
							maxLength: { value: 80, message: "Must be up to 80 in length" },
						})}
					/>
					<FormInput
						id="stock"
						type="number"
						title="Stock"
						step="1"
						error={errors?.stock?.message}
						defaultValue={initialValues.stock}
						register={register({
							required: { value: true, message: "The Stock is required" },
							min: { value: 0, message: "Stock must be greater than 0" },
						})}
					/>
					<FormInput
						id="price"
						type="number"
						title="Price"
						error={errors?.price?.message}
						defaultValue={initialValues.price}
						step="0.01"
						register={register({
							required: { value: true, message: "The price is required" },
							min: { value: 0, message: "Price must be greater than 0" },
							pattern: { value: /\d+(\.\d{1,2})?/, message: "Invalid price format" },
						})}
					/>

					{error && (
						<div className="text-left text-sm mt-5 text-red-600">
							<span>{error}</span>
						</div>
					)}

					<input
						id="submit"
						type="submit"
						autoComplete="on"
						className="cursor-pointer bg-green-500 hover:bg-green-400 w-full text-white rounded uppercase font-medium mt-5 p-2"
						value="Send"
					/>
				</form>
			</div>
		</div>
	);
};

export default ProductForm;

import React from "react";
import { useForm } from "react-hook-form";
import { IClient } from "../../types/IClients";
import { IUser, UserRole, UserStatus } from "../../types/IUser";
import FormInput from "../layout/FormInput";
import Select from "../layout/Select";

export interface UserFormProps {
	initialValues?: IUser;
	error?: string;
	onSubmit(user: IUser): Promise<void>;
}

const defaultValues = { email: "", firstname: "", lastname: "", role: UserRole.USER, status: UserStatus.INACTIVE };

const UserForm = ({ initialValues = { ...defaultValues }, onSubmit, error }: UserFormProps) => {
	const { register, handleSubmit, errors } = useForm();
	const onSubmitForm = (client: IClient) => onSubmit({ ...initialValues, ...client });

	return (
		<div className="flex justify-center mt-5">
			<div className="w-full max-w-lg">
				<form className="bg-white shadow-md px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit(onSubmitForm)}>
					<FormInput
						id="firstname"
						type="text"
						title="First name"
						error={errors?.firstname?.message}
						defaultValue={initialValues.firstname}
						register={register({
							required: { value: true, message: "The first name is required" },
							min: { value: 6, message: "Must be at least 6 long" },
							maxLength: { value: 80, message: "Must be up to 80 in length" },
						})}
					/>
					<FormInput
						id="lastname"
						type="text"
						title="Last name"
						error={errors?.lastname?.message}
						defaultValue={initialValues.lastname}
						register={register({
							required: { value: true, message: "The last name is required" },
							min: { value: 6, message: "Must be at least 6 long" },
							maxLength: { value: 80, message: "Must be up to 80 in length" },
						})}
					/>

					<Select
						id="role"
						options={[
							{ label: "Admin", value: "ADMIN" },
							{ label: "User", value: "USER" },
						]}
						defaultValue={initialValues?.role}
						register={register}
					/>

					<Select
						id="status"
						options={[
							{ label: "Active", value: "ACTIVE" },
							{ label: "Inactive", value: "INACTIVE" },
						]}
						defaultValue={initialValues?.status}
						register={register}
					/>

					<FormInput
						id="email"
						type="email"
						title="Email"
						error={errors?.email?.message}
						defaultValue={initialValues?.email}
						register={register({
							required: { value: true, message: "Email is required" },
							pattern: { value: /^\S+@\S+$/i, message: "The email does not have a valid format" },
						})}
					/>

					{error && (
						<div className="text-left text-sm mt-5 text-red-600">
							<span>{error}</span>
						</div>
					)}

					<input
						id="submit"
						type="submit"
						autoComplete="on"
						className="cursor-pointer bg-green-500 hover:bg-green-400 w-full text-white rounded uppercase font-medium mt-5 p-2"
						value="Send"
					/>
				</form>
			</div>
		</div>
	);
};

export default UserForm;

import React from "react";
import { IUser } from "../../../types/IUser";
import UserRow from "./UserRow";

export interface UsersTableProps {
	users?: IUser[];
	isLoading: boolean;
	error?: string;

	onDeleteUser(user: IUser);
	onUpdateUser(user: IUser);
}

const UsersTable = ({ users, isLoading, error, onDeleteUser, onUpdateUser }: UsersTableProps) => {
	if (isLoading) return <p>Loading...</p>;

	if (error) return <p>Error: {error}</p>;

	return (
		<table className="table-auto shadow-md mt-10 w-full w-lg">
			<thead className="bg-gray-800">
				<tr className="text-white">
					<th className="w-1/5 py-2">Name</th>
					<th className="w-1/5 py-2">Email</th>
					<th className="w-1/5 py-2">Role</th>
					<th className="w-1/5 py-2">Status</th>
					<th className="w-1/12 py-2">Update</th>
					<th className="w-1/12 py-2">Delete</th>
				</tr>
			</thead>
			<tbody className="bg-white">
				{users.map((user) => (
					<UserRow key={user.id} user={user} onDeleteUser={onDeleteUser} onUpdateUser={onUpdateUser} />
				))}
			</tbody>
		</table>
	);
};

export default UsersTable;

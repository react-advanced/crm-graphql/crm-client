import React from "react";
import { IUser } from "../../../types/IUser";

export interface UserRowProps {
	user: IUser;
	onDeleteUser(user: IUser);
	onUpdateUser(user: IUser);
}

const UserRow = ({ user, onDeleteUser, onUpdateUser }: UserRowProps) => {
	return (
		<tr>
			<td className="border px-4 py-2">{`${user.firstname} ${user.lastname}`}</td>
			<td className="border px-4 py-2">{user.email}</td>
			<td className="border px-4 py-2">{user.role}</td>
			<td className="border px-4 py-2">{user.status}</td>
			<td className="border px-4 py-2">
				<div className="w-full flex justify-center">
					<button type="button" className="text-blue-500" onClick={() => onUpdateUser(user)}>
						<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
							<path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z" />
						</svg>
					</button>
				</div>
			</td>
			<td className="border px-4 py-2">
				<div className="w-full flex justify-center">
					<button type="button" className="text-red-500" onClick={() => onDeleteUser(user)}>
						<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
							<path
								fillRule="evenodd"
								d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
								clipRule="evenodd"
							/>
						</svg>
					</button>
				</div>
			</td>
		</tr>
	);
};

export default UserRow;

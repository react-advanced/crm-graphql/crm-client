import React from "react";
import { IClient } from "../../../types/IClients";
import ClientRow from "./ClientRow";

export interface ClientsTableProps {
	clients?: IClient[];
	isLoading: boolean;
	error?: string;

	onDeleteClient(client: IClient);
	onUpdateClient(client: IClient);
}

const ClientsTable = ({ clients, isLoading, error, onDeleteClient, onUpdateClient }: ClientsTableProps) => {
	if (isLoading) return <p>Loading...</p>;

	if (error) return <p>Error: {error}</p>;

	return (
		<table className="table-auto shadow-md mt-10 w-full w-lg">
			<thead className="bg-gray-800">
				<tr className="text-white">
					<th className="w-1/5 py-2">Name</th>
					<th className="w-1/5 py-2">Company</th>
					<th className="w-1/5 py-2">Phone</th>
					<th className="w-1/5 py-2">Email</th>
					<th className="w-1/12 py-2">Update</th>
					<th className="w-1/12 py-2">Delete</th>
				</tr>
			</thead>
			<tbody className="bg-white">
				{clients.map((client) => (
					<ClientRow key={client.id} client={client} onDeleteClient={onDeleteClient} onUpdateClient={onUpdateClient} />
				))}
			</tbody>
		</table>
	);
};

export default ClientsTable;

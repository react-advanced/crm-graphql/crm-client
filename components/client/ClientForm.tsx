import React from "react";
import { useForm } from "react-hook-form";
import { IClient } from "../../types/IClients";
import FormInput from "../layout/FormInput";

export interface ClientFormProps {
	initialValues?: IClient;
	error?: string;
	onSubmit(client: IClient): Promise<void>;
}

const ClientForm = ({ initialValues = { company: "", email: "", firstname: "", lastname: "" }, onSubmit, error }: ClientFormProps) => {
	const { register, handleSubmit, errors } = useForm();
	const onSubmitForm = (client: IClient) => onSubmit({ ...initialValues, ...client });

	return (
		<div className="flex justify-center mt-5">
			<div className="w-full max-w-lg">
				<form className="bg-white shadow-md px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit(onSubmitForm)}>
					<FormInput
						id="firstname"
						type="text"
						title="First name"
						error={errors?.firstname?.message}
						defaultValue={initialValues.firstname}
						register={register({
							required: { value: true, message: "The first name is required" },
							min: { value: 6, message: "Must be at least 6 long" },
							maxLength: { value: 80, message: "Must be up to 80 in length" },
						})}
					/>
					<FormInput
						id="lastname"
						type="text"
						title="Last name"
						error={errors?.lastname?.message}
						defaultValue={initialValues.lastname}
						register={register({
							required: { value: true, message: "The last name is required" },
							min: { value: 6, message: "Must be at least 6 long" },
							maxLength: { value: 80, message: "Must be up to 80 in length" },
						})}
					/>
					<FormInput
						id="company"
						type="text"
						title="Company"
						error={errors?.company?.message}
						defaultValue={initialValues.company}
						register={register({
							required: { value: true, message: "The Company is required" },
							min: { value: 6, message: "Must be at least 3 long" },
							maxLength: { value: 80, message: "Must be up to 80 in length" },
						})}
					/>
					<FormInput
						id="phone"
						type="text"
						title="Phone"
						error={errors?.phone?.message}
						defaultValue={initialValues.phone}
						register={register({
							pattern: { value: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g, message: "The phone does not have a valid format" },
						})}
					/>
					<FormInput
						id="email"
						type="email"
						title="Email"
						error={errors?.email?.message}
						defaultValue={initialValues.email}
						register={register({
							required: { value: true, message: "Email is required" },
							pattern: { value: /^\S+@\S+$/i, message: "The email does not have a valid format" },
						})}
					/>

					{error && (
						<div className="text-left text-sm mt-5 text-red-600">
							<span>{error}</span>
						</div>
					)}

					<input
						id="submit"
						type="submit"
						autoComplete="on"
						className="cursor-pointer bg-green-500 hover:bg-green-400 w-full text-white rounded uppercase font-medium mt-5 p-2"
						value="Send"
					/>
				</form>
			</div>
		</div>
	);
};

export default ClientForm;

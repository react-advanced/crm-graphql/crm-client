import React from "react";
import { useGetUser } from "../../hooks/user/useGetUser";
import { useAuth } from "../auth/Authentication";

const Header = () => {
	const { logout } = useAuth();
	const { user, isLoading, error } = useGetUser();

	return (
		<div>
			<h1 className="flex justify-end px-2">
				{isLoading && "Loading..."}
				{error}
				{user && (
					<div>
						<span>{`${user.firstname} ${user.lastname}`}</span>
						<button
							className="mx-4 px-2 py-1 bg-blue-800 w-full sm:w-auto font-bold text-white text-xs rounded shadow-md"
							type="button"
							onClick={() => logout()}
						>
							logout
						</button>
					</div>
				)}
			</h1>
		</div>
	);
};

export default Header;

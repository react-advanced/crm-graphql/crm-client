import React from "react";
import Head from "./Head";

const LoadingScreen = () => {
	return (
		<div>
			<Head title="Loading..." />
			<div className="flex flex-col flex-wrap w-screen h-screen bg-gray-800 place-content-center justify-center">
				<div className="inline-block mx-5">
					<span className="relative">
						<span className="animate-ping fixed inline-flex h-10 w-10 rounded-full bg-pink-400 opacity-75"></span>
						<span className="relative inline-flex rounded-full h-10 w-10 bg-pink-500"></span>
					</span>
				</div>
				<div className="inline-block mx-auto">
					<span className="text-white pl-3">loading...</span>
				</div>
			</div>
		</div>
	);
};

export default LoadingScreen;

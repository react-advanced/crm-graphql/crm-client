import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

export const NavLink = ({ href, pathname, text }) => (
	<li className={pathname === href ? "bg-blue-800 p-2" : "p-2"}>
		<Link href={href}>
			<a className="text-white block">{text}</a>
		</Link>
	</li>
);

const Sidebar = () => {
	const { pathname } = useRouter();

	return (
		<aside className="bg-gray-800 sm:w-1/3 xl:w-1/5 sm:min-h-screen p-5">
			<div>
				<p className="text-white text-2xl font-bold">CRM Clients</p>
			</div>

			<nav className="mt-5 list-none">
				<NavLink href="/" pathname={pathname} text="Clients" />
				<NavLink href="/orders" pathname={pathname} text="Orders" />
				<NavLink href="/products" pathname={pathname} text="Products" />
				<NavLink href="/users" pathname={pathname} text="Users" />
			</nav>
		</aside>
	);
};

export default Sidebar;

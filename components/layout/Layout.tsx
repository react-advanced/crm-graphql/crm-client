import React from "react";
import Head from "./Head";
import Header from "./Header";
import Sidebar from "./Sidebar";

const Layout = ({ children }) => {
	return (
		<>
			<Head title="CRM" />

			<div className="bg-gray-100 min-h-screen">
				<div className="flex max-h-screen">
					<Sidebar />

					<main className="sm:w-2/3 xl:w-4/5 sm:min-h-screen overflow-y-auto p-5">
						<Header />
						{children}
					</main>
				</div>
			</div>
		</>
	);
};

export default Layout;

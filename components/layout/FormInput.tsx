import React from "react";
import { FormInputProps } from "../../types/FormInputProps";

const Alert = ({ message }) => {
	if (message) {
		return <div className="text-sm right-0 border-red-500 text-red-400 pt-1 px-2">{message}</div>;
	}

	return null;
};

const FormInput = ({ id, title, type, register, error, autoComplete, defaultValue, step }: FormInputProps) => {
	return (
		<div className="mb-4">
			<label className="block text-gray-700 text-sm font-bold mb-2" htmlFor={id}>
				{title}
			</label>
			<input
				id={id}
				name={id}
				type={type}
				ref={register}
				autoComplete={autoComplete}
				defaultValue={defaultValue}
				step={step || "any"}
				className="shadow appareance border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
			/>
			<Alert message={error} />
		</div>
	);
};

export default FormInput;

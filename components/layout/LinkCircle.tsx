import Link from "next/link";
import React from "react";

export interface LinkCircleProps {
	to: string;
}

const LinkCircle = ({ to }: LinkCircleProps) => {
	return (
		<Link href={to}>
			<button type="button" className="text-blue-800 hover:text-gray-800 focus:outline-none">
				<svg
					className="w-12 h-12 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110"
					fill="currentColor"
					viewBox="0 0 20 20"
					xmlns="http://www.w3.org/2000/svg"
				>
					<path
						fillRule="evenodd"
						d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z"
						clipRule="evenodd"
					/>
				</svg>
			</button>
		</Link>
	);
};

export default LinkCircle;

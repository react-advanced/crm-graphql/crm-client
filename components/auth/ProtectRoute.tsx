import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { PUBLIC_PATHNAMES } from "../../constants/routes";
import LoadingScreen from "../layout/LoadingScreen";
import { useAuth } from "./Authentication";

const ProtectRoute = ({ children }) => {
	const router = useRouter();
	const [loadingRoute, setLoadingRoute] = useState(true);
	const { isAuthenticated, loading: loadingAuth } = useAuth();

	const init = async () => {
		if (loadingAuth) return;

		if (isAuthenticated && PUBLIC_PATHNAMES.includes(router.pathname)) await router.push("/");

		if (!isAuthenticated && !PUBLIC_PATHNAMES.includes(router.pathname)) await router.push("/login");

		setLoadingRoute(false);
	};

	useEffect(() => {
		init();
	}, [router.pathname, isAuthenticated, loadingAuth]);

	if (loadingRoute) return <LoadingScreen />;

	if (!isAuthenticated && !PUBLIC_PATHNAMES.includes(router.pathname)) return <LoadingScreen />;

	return children;
};

export default ProtectRoute;

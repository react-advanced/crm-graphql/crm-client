import { useApolloClient, useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import { createContext, useContext, useEffect, useState } from "react";
import { AUTHENTICATE } from "../../graphql/authenticate";
import { SAVE_USER } from "../../graphql/users";
import { AuthResult, LoginInput, SignUpInput } from "../../types/Auth";
import { getAuthFromLocalStorage, setAuthInLocalStorage } from "../../utils/token";

const AuthContext = createContext({});

export const useAuth = (): AuthResult => useContext(AuthContext) as AuthResult;

export const AuthenticationProvider = ({ children }) => {
	const [authenticate] = useMutation(AUTHENTICATE);
	const [saveUser] = useMutation(SAVE_USER);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(null);
	const [token, setToken] = useState(null);
	const apolloClient = useApolloClient();
	const router = useRouter();

	useEffect(() => {
		const auth = getAuthFromLocalStorage();

		if (!auth || (auth?.expiresAt && Date.now() > parseInt(auth.expiresAt))) clearData();

		if (auth?.accessToken && auth.accessToken !== token) setToken(auth.accessToken);

		setLoading(false);
	});

	useEffect(() => {
		if (error) setError(null);
	}, [router.pathname]);

	const login = async ({ email, password }: LoginInput): Promise<void> => {
		setLoading(true);
		try {
			setToken(null);
			setAuthInLocalStorage(null);
			const { data } = await authenticate({ variables: { input: { email, password } } });
			setToken(data.authenticate.accessToken);
			setAuthInLocalStorage(data.authenticate);
			await router.push("/");
		} catch (e) {
			console.log(e);
			setError(e.message);
		}
		setLoading(false);
	};

	const signup = async ({ firstname, lastname, email, password }: SignUpInput): Promise<void> => {
		setLoading(true);
		try {
			await saveUser({
				variables: {
					input: {
						firstname,
						lastname,
						email,
						password,
					},
				},
			});
			await router.push("/login");
		} catch (e) {
			console.log(e);
			setError(e.message);
		}
		setLoading(false);
	};

	const logout = async () => {
		setLoading(true);
		await clearData();
		await router.push("/login");
		setLoading(false);
	};

	const clearData = async () => {
		setToken(null);
		setAuthInLocalStorage(null);
		await apolloClient.cache.reset();
	};

	return <AuthContext.Provider value={{ isAuthenticated: !!token, login, logout, signup, token, loading, error }}>{children}</AuthContext.Provider>;
};

import Link from "next/link";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { LoginInput } from "../../types/Auth";
import FormInput from "../layout/FormInput";
import Head from "../layout/Head";
import LoadingScreen from "../layout/LoadingScreen";
import { useAuth } from "./Authentication";

const Login = () => {
	const { login, error } = useAuth();
	const { register, handleSubmit, errors } = useForm();
	const [loading, setLoading] = useState(false);
	const onSubmit = (data: LoginInput) => {
		setLoading(true);
		login(data).finally(() => setLoading(false));
	};

	if (loading) return <LoadingScreen />;

	return (
		<div className="flex flex-wrap justify-around content-center min-h-screen bg-gray-800">
			<Head title="CRM - Login" />
			<div className="sm:w-screen xl:w-1/3">
				<p className="text-center text-white text-2xl font-white">CRM</p>

				<div className="flex justify-center mt-5">
					<div className="w-full max-w-sm">
						<form className="bg-white rounded shadow-md px-8 pt-6 pb-8 md-4" onSubmit={handleSubmit(onSubmit)}>
							<FormInput
								id="email"
								type="email"
								title="Email"
								error={errors?.email?.message}
								register={register({
									required: { value: true, message: "Email is required" },
									pattern: { value: /^\S+@\S+$/i, message: "The email does not have a valid format" },
								})}
							/>
							<FormInput
								id="password"
								type="password"
								title="Password"
								autoComplete="on"
								error={errors?.password?.message}
								register={register({
									required: { value: true, message: "Email is required" },
								})}
							/>

							<input
								id="submit"
								type="submit"
								autoComplete="on"
								className="cursor-pointer bg-green-500 hover:bg-green-400 w-full text-white rounded uppercase font-medium mt-5 p-2"
								value="Login"
							/>

							{error && (
								<div className="text-left text-sm mt-5 text-red-600">
									<span>{error}</span>
								</div>
							)}

							<div className="text-left text-sm mt-5">
								<span>Don't have an account yet? </span>
								<Link href="/signup">
									<a className="text-blue-500 hover:underline">Register now!</a>
								</Link>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Login;

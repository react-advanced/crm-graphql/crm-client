import Link from "next/link";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { SignUpInput } from "../../types/Auth";
import FormInput from "../layout/FormInput";
import Head from "../layout/Head";
import LoadingScreen from "../layout/LoadingScreen";
import { useAuth } from "./Authentication";

const SignUp = () => {
	const { signup, error } = useAuth();
	const { register, handleSubmit, errors } = useForm();
	const [loading, setLoading] = useState(false);
	const onSubmit = (data: SignUpInput) => {
		setLoading(true);
		signup(data).finally(() => setLoading(false));
	};

	if (loading) return <LoadingScreen />;

	return (
		<div className="flex flex-wrap justify-around content-center min-h-screen bg-gray-800">
			<Head title="CRM - SignUp" />
			<div className="sm:w-screen xl:w-1/3">
				<p className="text-center text-white text-2xl font-white">CRM</p>

				<div className="flex justify-center mt-5 mb-5">
					<div className="w-full max-w-sm">
						<form className="bg-white rounded shadow-md px-8 pt-6 pb-8 md-4" onSubmit={handleSubmit(onSubmit)}>
							<FormInput
								id="firstname"
								type="text"
								title="First name"
								error={errors?.firstname?.message}
								register={register({
									required: { value: true, message: "The first name is required" },
									min: { value: 6, message: "Must be at least 6 long" },
									maxLength: { value: 80, message: "Must be up to 80 in length" },
								})}
							/>
							<FormInput
								id="lastname"
								type="text"
								title="Last name"
								error={errors?.lastname?.message}
								register={register({
									required: { value: true, message: "The last name is required" },
									min: { value: 6, message: "Must be at least 6 long" },
									maxLength: { value: 80, message: "Must be up to 80 in length" },
								})}
							/>
							<FormInput
								id="email"
								type="email"
								title="Email"
								error={errors?.email?.message}
								register={register({
									required: { value: true, message: "Email is required" },
									pattern: { value: /^\S+@\S+$/i, message: "The email does not have a valid format" },
								})}
							/>
							<FormInput
								id="password"
								type="password"
								title="Password"
								error={errors?.password?.message}
								autoComplete="on"
								register={register({
									required: { value: true, message: "Email is required" },
									pattern: {
										value: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/i,
										message:
											"The password must have at least one lowercase character, another uppercase, " +
											"a number and must be at least 8 characters long.",
									},
								})}
							/>

							<input
								id="submit"
								type="submit"
								autoComplete="on"
								className="cursor-pointer bg-green-500 hover:bg-green-400 w-full text-white rounded uppercase font-medium mt-5 p-2"
								value="Register"
							/>

							{error && (
								<div className="text-left text-sm mt-5 text-red-600">
									<span>{error}</span>
								</div>
							)}

							<div className="text-left text-sm mt-5">
								<span>Do you already have an account? </span>
								<Link href="/login">
									<a className="text-blue-500 hover:underline">Log in!</a>
								</Link>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default SignUp;

import { ApolloClient, ApolloProvider, HttpLink, InMemoryCache } from "@apollo/client";
import fetch from "node-fetch";
import React from "react";

const client = new ApolloClient({
	cache: new InMemoryCache(),
	link: new HttpLink({
		uri: process.env.NEXT_PUBLIC_GRAPHQL_URI,
		fetch,
	}),
});

const ApolloCustomProvider = ({ children }) => <ApolloProvider client={client}>{children}</ApolloProvider>;

export default ApolloCustomProvider;

import moment from "moment";
import { IClient } from "../types/IClients";

export const mapClient = (client: IClient): IClient => ({ ...client, createAt: moment(parseInt(client.createAt)).format("DD-MM-YYYY hh:mm:ss") });

import moment from "moment";

export const parseDate = (date: string) => moment(parseInt(date)).format("DD-MM-YYYY hh:mm:ss");

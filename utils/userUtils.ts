import moment from 'moment';
import { IUser } from "../types/IUser";

export const mapUser = (user: IUser): IUser => ({ ...user, createAt: moment(parseInt(user.createAt)).format("DD-MM-YYYY hh:mm:ss") });
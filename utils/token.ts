import { IAuth } from "../types/IAuth";

export const getAuthFromLocalStorage = (): IAuth => {
	const auth = localStorage.getItem("AUTH");
	return auth ? JSON.parse(auth) : null;
};

export const setAuthInLocalStorage = (auth: IAuth) => localStorage.setItem("AUTH", JSON.stringify(auth));

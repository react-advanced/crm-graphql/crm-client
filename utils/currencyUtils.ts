export const toCurrency = (number: number): string => number.toLocaleString("es-AR", { style: "currency", currency: "ARS" });

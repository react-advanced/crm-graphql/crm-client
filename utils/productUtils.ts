import moment from "moment";
import { IProductQuantity } from "../types/IOrder";
import { IProduct } from "../types/IProduct";

export const mapProduct = (product: IProduct): IProduct => ({ ...product, createAt: moment(parseInt(product.createAt)).format("DD-MM-YYYY hh:mm:ss") });

export const compareProductQuantities = (a: IProductQuantity, b: IProductQuantity) => {
	if (a.product.name > b.product.name) return 1;
	if (a.product.name < b.product.name) return -1;
	return 0;
};

export const sortProductQuantities = (quantities: IProductQuantity[]) => [...quantities].sort(compareProductQuantities);

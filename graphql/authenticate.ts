import { gql } from "@apollo/client";

export const AUTHENTICATE = gql`
	mutation authenticate($input: AuthenticationInput!) {
		authenticate(input: $input) {
			accessToken
			createAt
			expiresAt
		}
	}
`;

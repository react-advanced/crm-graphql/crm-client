import { gql } from "@apollo/client";

export const GET_PRODUCT = gql`
	query getProduct($id: ID!) {
		getProduct(id: $id) {
			id
			name
			stock
			price
			createAt
		}
	}
`;

export const GET_PRODUCTS = gql`
	query getProducts {
		getProducts {
			id
			name
			stock
			price
			createAt
		}
	}
`;

export const SAVE_PRODUCT = gql`
	mutation saveProduct($input: ProductInput!) {
		saveProduct(input: $input) {
			id
			name
			stock
			price
			createAt
		}
	}
`;

export const UPDATE_PRODUCT = gql`
	mutation updateProduct($id: ID!, $input: ProductInput!) {
		updateProduct(id: $id, input: $input) {
			id
			name
			stock
			price
			createAt
		}
	}
`;

export const DELETE_PRODUCT = gql`
	mutation deleteProduct($id: ID!) {
		deleteProduct(id: $id) {
			id
			name
			stock
			price
			createAt
		}
	}
`;

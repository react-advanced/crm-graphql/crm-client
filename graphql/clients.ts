import { gql } from "@apollo/client";

export const GET_CLIENT = gql`
	query getClient($id: ID!) {
		getClient(id: $id) {
			id
			firstname
			lastname
			email
			phone
			company
			createAt
			seller
		}
	}
`;

export const GET_CLIENTS = gql`
	query getClients {
		getClients {
			id
			firstname
			lastname
			email
			phone
			company
			createAt
			seller
		}
	}
`;

export const SAVE_CLIENT = gql`
	mutation saveClient($input: ClientInput!) {
		saveClient(input: $input) {
			id
			firstname
			lastname
			email
			phone
			company
			createAt
			seller
		}
	}
`;

export const UPDATE_CLIENT = gql`
	mutation updateClient($id: ID!, $input: ClientInput!) {
		updateClient(id: $id, input: $input) {
			id
			firstname
			lastname
			email
			phone
			company
			createAt
			seller
		}
	}
`;

export const DELETE_CLIENT = gql`
	mutation deleteClient($id: ID!) {
		deleteClient(id: $id) {
			id
			firstname
			lastname
			email
			phone
			company
			createAt
			seller
		}
	}
`;

import { gql } from "@apollo/client";

export const GET_ORDERS_BY_USER = gql`
	query getOrdersByUser {
		getOrdersByUser {
			id
			productOrders {
				id
				quantity
			}
			total
			client {
				id
				firstname
				lastname
				email
				phone
				company
				createAt
				seller
			}
			user
			status
			createAt
		}
	}
`;

export const SAVE_ORDER = gql`
	mutation saveOrder($input: OrderInput!) {
		saveOrder(input: $input) {
			id
			productOrders {
				id
				quantity
			}
			total
			client
			user
			status
			createAt
		}
	}
`;

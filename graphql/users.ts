import { gql } from "@apollo/client";

export const SAVE_USER = gql`
	mutation saveUser($input: UserInput!) {
		saveUser(input: $input) {
			id
			firstname
			lastname
			email
			createAt
		}
	}
`;

export const GET_USERS = gql`
	query getUsers {
		getUsers {
			id
			firstname
			lastname
			email
			role
			status
			createAt
		}
	}
`;

export const GET_USER = gql`
	query getUser {
		getUser {
			id
			firstname
			lastname
			email
			role
			status
			createAt
		}
	}
`;

export const GET_USER_BY_ID = gql`
	query getUserById($id: ID!) {
		getUserById(id: $id) {
			id
			firstname
			lastname
			email
			role
			status
			createAt
		}
	}
`;

export const UPDATE_USER = gql`
	mutation updateUser($id: ID!, $input: UserUpdateInput!) {
		updateUser(id: $id, input: $input) {
			id
			firstname
			lastname
			email
			role
			status
			createAt
		}
	}
`;

export const DELETE_USER = gql`
	mutation deleteUser($id: ID!) {
		deleteUser(id: $id) {
			id
			firstname
			lastname
			email
			role
			status
			createAt
		}
	}
`;
